<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

/**
 * Class Configuration
 * @package PETL\Standard\Common
 */
class Configuration
{
    const OPTION_DELIMITER = ':';

    /**
     * @param $target
     * @param array $configuration
     * @param bool $override
     * @return mixed
     */
    public static function apply($target, array $configuration = [], $override = false)
    {

        if ($configuration instanceof \JsonSerializable) {
            $configuration = $configuration->jsonSerialize();
        }
        if (
            !$override && $target instanceof PublicOptionsInterface
        ) {
            $configuration =
                array_intersect_key(
                    $configuration,
                    array_filter(
                        $target->getPublicOptions()
                    )
                );
        }

        foreach ($configuration as $option => $value) {

            $propertyName             =
                ucfirst(
                    self::getPropertyName($option)
                );
            $subjectDelimiterPosition =
                stripos(
                    $propertyName,
                    ':'
                );

            if ($subjectDelimiterPosition !== false) {

                $subjectName         = substr($propertyName, 0, $subjectDelimiterPosition);
                $subjectPropertyName = substr($propertyName, $subjectDelimiterPosition + 1);

                self::apply(
                    $target,
                    [
                        $subjectName => [
                            $subjectPropertyName => $value,
                        ],
                    ],
                    $override
                );
            } else {

                $setterName = 'set' . $propertyName;
                $getterName = 'get' . $propertyName;

                switch (true) {
                    case
                        is_array($value)
                        && method_exists($target, $getterName)
                        && is_object($target->$getterName())
                    :
                        self::apply($target->$getterName(), $value, $override);
                        break;
                    case method_exists($target, $setterName):
                        $target->$setterName($value);
                        break;
                    default:
                        break;
                }
            }
        }

        return $target;
    }

    /**
     * @param        $configuration
     * @param null $option
     * @param null $default
     * @param string $delimiter
     * @return mixed|null
     */
    public static function getOption($configuration, $option = null, $default = null, $delimiter = self::OPTION_DELIMITER)
    {
        if (!$option) {
            return $configuration ?: $default;
        }
        $options = $configuration;
        foreach (explode($delimiter, $option) as $optionName) {

            switch (true) {
                case is_null($optionName):
                    return $options ?: $default;
                case (
                    '' === $optionName
                    && is_array($options)
                ):
                    $options = reset($options);
                    break;
                case (
                    is_array($options)
                    && array_key_exists($optionName, $options)
                ):
                    $options = $options[$optionName];
                    break;
                case(
                    is_object($options)
                    && method_exists(
                        $options,
                        $getter = 'get' . ucfirst(self::getPropertyName($optionName))
                    )
                ):
                    $options = $options->$getter();
                    break;
                default:
                    return $default;
            }
        }

        return $options;
    }

    /**
     * @param $name
     * @return bool|string
     */
    public static function getPropertyName($name)
    {

        if (!is_string($name)) {
            return false;
        }

        if (stripos($name, '_') === false) {
            return $name;
        }

        return
            self::underscore2camelCase(
                $name
            );
    }

    /**
     * @param string $text
     * @return string
     */
    public static function underscore2camelCase($text)
    {

        return
            lcfirst(
                str_replace(
                    ' ',
                    '',
                    ucwords(
                        implode(
                            ' ',
                            explode(
                                '_',
                                $text
                            )
                        )
                    )
                )
            );
    }
}

