<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */


namespace PETL\Standard\Common;


interface MessengerInterface
{

	public function &getMessages();

	public function getLastMessage();

	public function addMessage($message);

	public function setMessages(array $messages);

	public function clearMessages();

	public function mergeMessagesFromObject($object);

} 