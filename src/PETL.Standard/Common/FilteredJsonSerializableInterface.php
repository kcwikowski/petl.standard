<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

/**
 * Interface InitializableInterface
 *
 * Provides interface for objects that should be initialized before use and shutdown afterwards.
 *
 * @package PETL
 */
interface FilteredJsonSerializableInterface
{
	/**
	 * @param array $data
	 */
    public function filterJsonSerialize(array &$data);

}
