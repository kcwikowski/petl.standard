<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

class Util
{
    const SUFFIX_FLAT       = 'F';
    const SUFFIX_JSON       = 'J';
    const SUFFIX_COMPRESSED = 'C';

    /**
     * @return int
     */
    public static function time()
    {
        return
            floor(
                self::now()
            );
    }

    /**
     * @return float
     */
    public static function now()
    {

        list($uSec, $sec) = explode(" ", microtime());
        $microTime = ((float)$uSec + (float)$sec);

        return $microTime;
    }

    /**
     * @param      $data
     * @param bool $encodeOnly
     * @param bool $forceArchive
     * @return bool|string
     */
    public static function safeZip($data, $encodeOnly = false, $forceArchive = false)
    {
        $archive = self::safeString($data);

        if (!$archive) {
            return false;
        }
        if ($encodeOnly) {
            return $archive . self::SUFFIX_FLAT;
        }
        $compressedData = base64_encode(gzcompress($archive));
        if (
            !$forceArchive
            && strlen($archive) <= strlen($compressedData)
        ) {
            return $archive . self::SUFFIX_FLAT;
        }

        return $compressedData . self::SUFFIX_COMPRESSED;
    }

    /**
     * @param mixed $data
     * @return string
     */
    public static function safeString($data)
    {
        if (!$data) {
            return false;
        }
        if (is_string($data)) {
            return $data . self::SUFFIX_FLAT;
        }

        $json = json_encode($data) . self::SUFFIX_JSON;

        return
            JSON_ERROR_NONE == json_last_error()
                ?
                $json
                :
                false;
    }

    /**
     * @param $archive
     * @return bool|mixed|string
     */
    public static function safeUnzip($archive)
    {
        if (!is_string($archive)) {
            return false;
        }
        $storageFormat = substr($archive, -1);

        switch (true) {
            case self::SUFFIX_COMPRESSED == $storageFormat:
                $data = gzuncompress(base64_decode(substr($archive, 0, -1)));
                if (!$data) {
                    return false;
                }
                break;
            case self::SUFFIX_FLAT == $storageFormat:
                $data = substr($archive, 0, -1);
                break;
            default:
                $data =& $archive;
                break;
        }
        $data = self::safeUnstring($data);

        return $data;
    }

    /**
     * @param string $string
     * @return mixed
     */
    public static function safeUnstring($string)
    {
        if (!is_string($string)) {
            return false;
        }
        $storageFormat = substr($string, -1);

        switch (true) {
            case self::SUFFIX_JSON == $storageFormat:
                $data = json_decode(substr($string, 0, -1), true);
                if (JSON_ERROR_NONE != json_last_error()) {
                    return false;
                }

                return $data;
                break;
            case self::SUFFIX_FLAT == $storageFormat:
                return substr($string, 0, -1);
                break;
            default:
                return $string;
                break;
        }
    }
    /**
     * @param mixed $data
     * @return string
     */
    public static function toJB64($data)
    {

        return base64_encode(json_encode($data));
    }

    /**
     * @param      $data
     * @param bool $status
     * @return mixed
     */
    public static function fromJB64($data, &$status = false)
    {

        $data   = json_decode(base64_decode($data), true);
        $status = json_last_error() === JSON_ERROR_NONE;

        return
            $status ? $data : null;
    }

    /**
     * @param mixed $test
     * @param mixed $default
     * @return mixed
     */
    public static function ifNull($test, $default)
    {
        return is_null($test) ? $default : $test;
    }

    /**
     * @param mixed $test
     * @param mixed $default
     * @return mixed
     */
    public static function ifNotScalar($test, $default)
    {
        return is_scalar($test) ? $test : $default;
    }

    /**
     * @param string $pattern
     * @param int $flags
     * @return array
     */
    public static function rglob($pattern, $flags = 0)
    {

        $files = glob($pattern, $flags);

        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {

            $files = array_merge($files, static::rglob($dir . '/' . basename($pattern), $flags));
        }

        return $files;
    }

    /**
     * @param &$var
     * @return array
     */
    public static function arrify(&$var)
    {

        if (!is_array($var)) {
            $var = is_null($var) ? [] : [$var];
        }

        return $var;
    }

    /**
     * @return float
     */
    public static function randF()
    {
        return (float)rand() / (float)getrandmax();
    }
}