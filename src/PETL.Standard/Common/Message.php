<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */


namespace PETL\Standard\Common;


class Message implements \JsonSerializable
{
	use JsonSerializableTrait;

	const INFO    = 'info';
	const SUCCESS = 'success';
	const WARNING = 'warning';
	const ERROR   = 'error';

	const TYPES = [
		self::INFO    => self::INFO,
		self::SUCCESS => self::SUCCESS,
		self::WARNING => self::WARNING,
		self::ERROR   => self::ERROR,
	];
	/**
	 * @var string
	 */
	protected $type;
	/**
	 * @var string
	 */
	protected $text;
	/**
	 * @var float
	 */
	protected $timestamp;

	/**
	 * Message constructor.
	 *
	 * @param        $text
	 * @param string $type
	 * @param null   $timestamp
	 */
	public function __construct($text = '', $type = self::INFO, $timestamp = null)
	{

		$this
			->setTimestamp($timestamp ?: Util::now())
			->setText($text)
			->setType($type);
	}

	/**
	 * @param string $text
	 * @return Message
	 */
	public static function info($text)
	{

		return new self($text, self::INFO);
	}

	/**
	 * @param string $text
	 * @return Message
	 */
	public static function success($text)
	{

		return new self($text, self::SUCCESS);
	}

	/**
	 * @param string $text
	 * @return Message
	 */
	public static function warning($text)
	{

		return new self($text, self::WARNING);
	}

	/**
	 * @param string $text
	 * @return Message
	 */
	public static function error($text)
	{

		return new self($text, self::ERROR);
	}

	function __toString()
	{
		return $this->getType() . ':' . $this->getText();
	}

	/**
	 * @return string
	 */
	public function getType()
	{

		return $this->type;
	}

    /**
     * @param $type
     * @return $this
     */
	public function setType($type)
	{

		$this->type = $type;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getText()
	{

		return $this->text;
	}

    /**
     * @param $text
     * @return $this
     */
	public function setText($text)
	{

		$this->text = $text;

		return $this;
	}

	/**
	 * @return float
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * @param float $timestamp
	 * @return self
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;

		return $this;
	}


}