<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

trait JsonSerializableTrait
{
    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *       which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {

        $data = $this->jsonSerializeUnfiltered();

        if ($this instanceof FilteredJsonSerializableInterface) {
            $this->filterJsonSerialize($data);
        }

        return $data;
    }

    /**
     * @return array
     */
    function jsonSerializeUnfiltered()
    {

        return get_object_vars($this);
    }
} 