<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

trait MessengerTrait
{
    /**
     * @var string[]|Message[]
     */
    protected $messages = [];

    /**
     * @return mixed
     */
    public function getLastMessage()
    {

        return end($this->messages);
    }

    /**
     * @return $this
     */
    public function clearMessages()
    {

        $this->messages = [];

        return $this;
    }

    /**
     * @return array
     */
    public function &getMessages()
    {

        return $this->messages;
    }

    /**
     * @param array $messages
     * @return $this
     */
    public function setMessages(array $messages)
    {

        return
            $this
                ->clearMessages()
                ->addMessages($messages);
    }

    /**
     * @param object $object
     * @param int $depth
     * @return $this
     */
    public function mergeMessagesFromObject($object, $depth = 0)
    {

        switch (true) {
            case $object instanceof MessengerInterface:
                $this->addMessages($object->getMessages());
                $object->clearMessages();
                break;
            case $object instanceof Message:
                $this->addMessage($object);
                break;
            case $object instanceof \Exception:
                $this->addMessage($object->getMessage());
                if ($object->getPrevious() && $depth < 10) {
                    $this->mergeMessagesFromObject($object->getPrevious(), $depth + 1);
                }
                break;
            default:
                break;
        }

        return $this;
    }

    /**
     * @param $message
     * @return self
     */
    public function addMessage($message)
    {
        if ($message = $this->parseMessage($message)) {
            $this->getMessages()[] = $message;
        }

        return $this;
    }

    /**
     * @param $message
     * @return bool|mixed|Message
     */
    protected function parseMessage($message)
    {
        switch (true) {
            case $message instanceof Message:
                return $message;
                break;
            case is_string($message):
                return new Message($message);
                break;
            case is_array($message):
                return
                    Configuration::apply(
                        new Message(),
                        $message
                    );
                break;
        }

        return false;
    }

    /**
     * @param array $messages
     * @return self
     */
    protected function addMessages(array $messages)
    {

        foreach ($messages as $message) {

            $this->addMessage($message);
        }

        return $this;
    }
}