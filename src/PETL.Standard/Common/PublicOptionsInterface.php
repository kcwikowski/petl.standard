<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Common;

/**
 * Interface PublicOptionsInterface
 * @package PETL\Common\Config
 */
interface PublicOptionsInterface
{
	/**
	 * @return string[]
	 */
	public function getPublicOptions();
}