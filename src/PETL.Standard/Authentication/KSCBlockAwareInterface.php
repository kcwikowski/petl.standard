<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */
namespace PETL\Standard\Authentication;

interface KSCBlockAwareInterface
{
    /**
     * @return KSCBlock
     */
    public function getKsc();

    /**
     * @param $ksc
     * @param bool $parse
     * @return static
     */
    public function setKsc($ksc, $parse = false);
}