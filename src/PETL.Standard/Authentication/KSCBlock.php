<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;

use PETL\Standard\Common\Configuration;
use PETL\Standard\Common\JsonSerializableTrait;

/**
 * Class KSCBlock
 *
 * Key-Secret-Cipher block class.
 *
 *
 * @package PETL\Common\Auth
 */
class KSCBlock implements \JsonSerializable
{
    use JsonSerializableTrait;
    /**
     * @var array
     */
    protected $credentials;
    /**
     * @var bool
     */
    protected $keyRotation;

    /**
     * KSCBlock constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        Configuration::apply($this, $options);
    }

    /**
     * @return string
     */
    public function getRandomKey()
    {

        $keys = array_keys($this->getCredentials());

        if ($this->isKeyRotation()) {

            $sliceSize = floor(count($keys) / 3);
            $keys      = array_slice($keys, $sliceSize, $sliceSize);
        }

        shuffle($keys);

        return array_shift($keys);
    }

    /**
     * @param $key
     * @return string
     */
    public function getSecret($key)
    {

        $secret = sha1(rand());

        if (array_key_exists($key, $this->getCredentials())) {
            $secret = $this->getCredentials()[$key];
        }

        if (!is_scalar($secret)) {
            $secret = json_encode($secret);
        }

        return $secret;
    }

    /**
     * @return array
     */
    public function &getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @param array $credentials
     * @return static
     */
    public function setCredentials($credentials)
    {
        $this->credentials = $credentials;

        return $this;
    }

    /**
     * @return bool
     */
    public function isKeyRotation()
    {
        return $this->keyRotation;
    }

    /**
     * @param bool $keyRotation
     * @return static
     */
    public function setKeyRotation($keyRotation)
    {
        $this->keyRotation = $keyRotation;

        return $this;
    }

    public function __debugInfo()
    {
        if (is_array($this->getCredentials())) {
            return [
                'keyRotation' => $this->isKeyRotation(),
                'credentials' => 'Keys count: ' . count($this->getCredentials()),
            ];
        } else {
            return get_object_vars($this);
        }
    }
}