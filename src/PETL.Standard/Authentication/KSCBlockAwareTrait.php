<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;

trait KSCBlockAwareTrait
{
    /**
     * @var KSCBlock
     */
    protected $ksc;

    /**
     * @return KSCBlock
     */
    public function getKsc()
    {
        if (is_null($this->ksc)) {
            $this->ksc = new KSCBlock();
        }
        if ($this->ksc instanceof KSCLazyLoader) {
            $this->ksc = $this->ksc->load();
        }

        return $this->ksc;
    }

    /**
     * @param $ksc
     * @param bool $parse
     * @return static
     */
    public function setKsc($ksc, $parse = false)
    {
        if (
            $parse
            && is_string($ksc)
        ) {
            $ksc = KSCLoader::loadFromString($ksc);
        }

        $this->ksc = $ksc;

        return $this;
    }
}