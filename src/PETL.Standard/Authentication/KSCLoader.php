<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;

use PETL\Standard\Common\Util;
use PETL\Standard\Storage\CacheAwareTrait;
use PETL\Standard\Storage\CacheInterface;

/**
 * Class KSCLoader
 * @package PETL\Standard\Authentication
 */
class KSCLoader
{
    use CacheAwareTrait;
    const DEFAULT_KEY_LENGTH    = 12;
    const DEFAULT_SECRET_LENGTH = 20;
    /**
     * @var string
     */
    protected $path;

    /**
     * KSCLoader constructor.
     * @param                     $path
     * @param CacheInterface|null $cache
     * @throws \Exception
     */
    public function __construct($path, $cache = null)
    {
        if (!is_string($path)) {
            throw new \Exception(
                sprintf(
                    "%s invalid path: %s",
                    self::class,
                    $path
                )
            );
        }

        $path = realpath($path);

        if (!(is_string($path) && is_readable($path))) {
            throw new \Exception(
                sprintf(
                    "%s path is not readable: %s",
                    self::class,
                    $path
                )
            );
        }

        $this
            ->setPath($path)
            ->setCache($cache);
    }

    /**
     * @param $kscBlock
     * @param int $keyLength
     * @param int $secretLength
     * @return bool|KSCBlock
     */
    public static function loadFromString(
        $kscBlock,
        $keyLength = self::DEFAULT_KEY_LENGTH,
        $secretLength = self::DEFAULT_SECRET_LENGTH
    )
    {
        return
            static::create(
                self::parse(
                    $kscBlock,
                    $keyLength,
                    $secretLength
                )
            );
    }

    /**
     * @param $kscBlock
     * @param int $keyLength
     * @param int $secretLength
     * @return array|bool
     */
    public static function parse(
        $kscBlock,
        $keyLength = self::DEFAULT_KEY_LENGTH,
        $secretLength = self::DEFAULT_SECRET_LENGTH
    )
    {

        $offset = 0;
        if ($keyLength < 0) {
            $keyLength    = ord($kscBlock[0]) - 32;
            $secretLength = ord($kscBlock[1]) - 32;
            $offset       = 2;
        }

        if ($keyLength == 0) {
            return
                [self::class => substr($kscBlock, $offset)];
        }
        $totalChunkSize = $keyLength + $secretLength;
        $kscBlockSize   = strlen($kscBlock) - $offset;
        $credentials    = [];

        if (0 !== $kscBlockSize % $totalChunkSize) {

            return false;
        }
        $iMax = (int)($kscBlockSize / $totalChunkSize);

        for ($i = 0; $i < $iMax; $i++) {
            $chunk = substr($kscBlock, $offset + $i * $totalChunkSize, $totalChunkSize);
            $key   = substr($chunk, 0, $keyLength);

            if (array_key_exists($key, $credentials)) {
                continue;
            }
            $credentials[$key] = substr($chunk, -$secretLength);
        }

        return $credentials;
    }

    /**
     * @param $credentials
     * @return KSCBlock
     */
    public static function create($credentials)
    {
        return
            (new KSCBlock())
                ->setCredentials($credentials);
    }

    /**
     * @param $name
     * @param int $customKeyLength
     * @param int $customSecretLength
     * @return KSCLazyLoader
     */
    public function lazyLoad($name, $customKeyLength = self::DEFAULT_KEY_LENGTH, $customSecretLength = self::DEFAULT_SECRET_LENGTH)
    {
        return
            (new KSCLazyLoader())
                ->setKscLoader($this)
                ->setName($name)
                ->setKeyLength($customKeyLength)
                ->setSecretLength($customSecretLength);
    }

    /**
     * @param $name
     * @param null $customKeyLength
     * @param null $customSecretLength
     * @return bool|KSCBlock
     */
    public function load($name, $customKeyLength = null, $customSecretLength = null)
    {
        $cKey = md5($name);

        if ($this->getCache()) {
            $credentials =
                $this->getCache()->load(
                    $cKey,
                    $status
                );
            if ($status) {
                return
                    static::create(
                        $credentials
                    );
            }
        }

        $kscBlock    = file_get_contents($this->geFilePath($name));
        $credentials =
            $this->parse(
                $kscBlock,
                Util::ifNull($customKeyLength, self::DEFAULT_KEY_LENGTH),
                Util::ifNull($customSecretLength, self::DEFAULT_SECRET_LENGTH)
            );

        if (!$credentials) {
            return false;
        }

        if ($this->getCache()) {
            $this->getCache()->store(
                $cKey,
                0,
                $credentials
            );
        }

        return
            static::create(
                $credentials
            );
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param $name
     * @return string
     */
    protected function geFilePath($name)
    {
        return $this->getPath() . DIRECTORY_SEPARATOR . $name . '.ksc';
    }
}