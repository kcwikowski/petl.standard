<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;

use PETL\Standard\Common\Configuration;
use PETL\Standard\Common\JsonSerializableTrait;

class SecureData implements \JsonSerializable
{
    use JsonSerializableTrait, IdentityAwareTrait;
    const SALT_LENGTH = 32;
    /**
     * @var string
     */
    protected $data;
    /**
     * @var string
     */
    protected $key;
    /**
     * @var string
     */
    protected $salt;
    /**
     * @var string
     */
    protected $time;
    /**
     * @var string
     */
    protected $hash;
    /**
     * @var bool
     */
    protected $encrypted;
    /**
     * @var bool
     */
    protected $compressed;
    /**
     * @var bool
     */
    protected $signed;

    public function __construct(array $data = [])
    {
        if ($data) {
            Configuration::apply($this, $data);
        }
    }

    /**
     * @return bool
     */
    public function preValidate()
    {
        return
            $this->getKey()
            && $this->getTime()
            && $this->getHash()
            && $this->getData()
            && (
                $this->isEncrypted()
                || $this->isSigned()
            );
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     * @return self
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEncrypted()
    {
        return $this->encrypted;
    }

    /**
     * @param bool $encrypted
     * @return self
     */
    public function setEncrypted($encrypted)
    {
        $this->encrypted = $encrypted;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCompressed()
    {
        return $this->compressed;
    }

    /**
     * @param bool $compressed
     * @return self
     */
    public function setCompressed($compressed)
    {
        $this->compressed = $compressed;

        return $this;
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $time
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return self
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSigned()
    {
        return $this->signed;
    }

    /**
     * @param bool $signed
     * @return self
     */
    public function setSigned($signed)
    {
        $this->signed = $signed;

        return $this;
    }
}