<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;

use PETL\Standard\Common\Configuration;
use PETL\Standard\Common\JsonSerializableTrait;

class Identity implements \JsonSerializable
{
    use JsonSerializableTrait;
    const SALT_LENGTH = 32;
    /**
     * @var string
     */
    protected $account;
    /**
     * @var string
     */
    protected $apiUser;

    public function __construct(array $data = [])
    {
        if ($data) {
            Configuration::apply($this, $data);
        }
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return static
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiUser()
    {
        return $this->apiUser;
    }

    /**
     * @param string $apiUser
     * @return static
     */
    public function setApiUser($apiUser)
    {
        $this->apiUser = $apiUser;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return
            $this->getAccount() . Configuration::OPTION_DELIMITER . $this->getApiUser();
    }
}