<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard\Authentication;


trait IdentityAwareTrait
{
    /**
     * @var Identity
     */
    protected $identity;

    /**
     * @return Identity
     */
    public function getIdentity()
    {
        if (is_null($this->identity)) {
            $this->identity = new Identity();
        }

        return $this->identity;
    }

    /**
     * @param Identity $identity
     * @return static
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;

        return $this;
    }
}