<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */
namespace PETL\Standard\Authentication;

/**
 * Class KSCLazyLoader
 * @package PETL\Standard\Authentication
 */
class KSCLazyLoader
{
    /**
     * @var KSCLoader
     */
    protected $kscLoader;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var int
     */
    protected $keyLength;
    /**
     * @var int
     */
    protected $secretLength;

    /**
     * @return array|bool|mixed
     */
    public function load()
    {
        return
            $this->getKscLoader()->load(
                $this->getName(),
                $this->getKeyLength(),
                $this->getSecretLength()
            );
    }

    /**
     * @return KSCLoader
     */
    public function getKscLoader()
    {
        return $this->kscLoader;
    }

    /**
     * @param KSCLoader $kscLoader
     * @return static
     */
    public function setKscLoader($kscLoader)
    {
        $this->kscLoader = $kscLoader;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return static
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getKeyLength()
    {
        return $this->keyLength;
    }

    /**
     * @param int $keyLength
     * @return static
     */
    public function setKeyLength($keyLength)
    {
        $this->keyLength = $keyLength;

        return $this;
    }

    /**
     * @return int
     */
    public function getSecretLength()
    {
        return $this->secretLength;
    }

    /**
     * @param int $secretLength
     * @return static
     */
    public function setSecretLength($secretLength)
    {
        $this->secretLength = $secretLength;

        return $this;
    }
}