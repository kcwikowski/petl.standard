<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */
namespace PETL\Standard\Storage;

trait CacheAwareTrait
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @return CacheInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param CacheInterface $cache
     * @return self
     */
    public function setCache($cache)
    {
        $this->cache = $cache;

        return $this;
    }
}