<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */


namespace PETL\Standard\Storage;


interface CacheInterface
{
    /**
     * @param $key
     * @param $TTL
     * @param null $data
     * @return mixed
     */
	public function store($key, $TTL, $data = null);

    /**
     * @param $key
     * @param null $status
     * @return mixed
     */
	public function load($key, &$status = null);

}