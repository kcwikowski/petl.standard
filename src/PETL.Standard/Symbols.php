<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard;

class Symbols
{
    const SDK_HTTP_HEADER = 'PETL-SDK';
    const SDK_POST_FIELD  = 'payload';
}