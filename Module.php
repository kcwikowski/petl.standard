<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Standard;

class Module
{
//    public function getServiceConfig()
//    {
//
//        return [
//            'factories' => [
//
//                AccountMetric::class    => function ($sm) {
//
//                    $config  = $sm->get('config');
//                    $options = Configuration::getOption($config, 'PETL.FlowStat:Repositories:AccountMetric', []);
//
//                    $instance = new AccountMetric($options);
//
//                    return $instance;
//                },
//            ],
//        ];
//    }

    public function getConfig()
    {

        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {

        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
